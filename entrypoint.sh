#!/bin/bash

set -euo pipefail
[[ ${DEBUG:-} ]] && set -x

# save reference to stdout as fd 3 then merge stdout to stderr for duration of script (keep stdout clean for the optional hostscript at the end)
exec 3>&1 1>&2

echo() {
    msg="$1"
    builtin echo "binstaller: ${msg}"
}
die() {
    msg="$1"
    code="${2:-1}"
    echo "$msg"
    exit "$code"
}

archive="$1"

[[ ${DEBUG:-} ]] && verbose_flag='v' || verbose_flag=

echo "Fetching archive from ${archive}"
curl -fSL -o archive.tgz "${archive}"

echo "Unpacking archive"
tar  -x${verbose_flag}zf archive.tgz

[[ -f sig.gpg.asc ]] || die 'Signature file not found'
[[ -f pack.tar ]] || die 'Pack file not found'

echo "Verifying GPG signature"
gpg --verify sig.gpg.asc pack.tar

echo "Unpacking pack.tar"
mkdir -${verbose_flag}p pack
tar -C pack -${verbose_flag}xf pack.tar
cd ./pack/

MANIFEST=manifest.json

[[ -f $MANIFEST ]] || die 'Manifest file not found in pack'

IFS="$(printf '\n\r')"

echo "Installing binaries..."
_bin_target="$(realpath -m "/rootfs/${BIN_TARGET:-/opt/bin}/")"
install_opts=( --backup=numbered --preserve-timestamps -D --target-directory="${_bin_target}" )
[[ ${DEBUG:-} ]] && install_opts+=( --verbose )
for bin in $(jq -r '.binaries[]' "${MANIFEST}"); do
    echo "Installing ${bin}"
    install "${install_opts[@]}" "${bin}"
done
echo "Done"

echo "Running augtool"
augtool_opts=( --root /rootfs --backup )
[[ ${DEBUG:-} ]] && augtool_opts+=( --echo )
for augfile in $(jq -r '.augtool[]' "${MANIFEST}"); do
    echo "Augtool ${augfile}"
    sed "s:\${BIN_TARGET}:${BIN_TARGET}:" < "$augfile" | augtool "${augtool_opts[@]}"
done
echo "Done"

hostScript="$(jq -r '.["host-script"]' "${MANIFEST}")"
if [[ ${hostScript} != null ]]; then
    echo "Host script:"
    printf 'PS4="host+ "\n' >&3
    [[ ${DEBUG:-} ]] && printf 'set -x\n' >&3
    cat ${hostScript} >&3
fi
