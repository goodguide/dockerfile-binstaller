FROM alpine:latest

RUN apk add --update --no-cache \
      augeas \
      bash \
      coreutils \
      curl \
      gnupg \
      jq

COPY gpg-keys/* /var/lib/gg-gpg-keys/
RUN /var/lib/gg-gpg-keys/import.sh

RUN mkdir /workspace
WORKDIR /workspace

COPY entrypoint.sh /bin/entrypoint
ENTRYPOINT ["/bin/entrypoint"]
