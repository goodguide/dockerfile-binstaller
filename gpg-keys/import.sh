#!/bin/bash

set -euo pipefail
[[ ${DEBUG:-} ]] && set -x

gpg --import $(dirname $0)/*.asc
gpg --list-keys --fingerprint --with-colons \
    | sed -E -n -e 's/^fpr:::::::::([0-9A-F]+):$/\1:6:/p' \
    | gpg --import-ownertrust
